import csv

DEBUG = True

def loadCsv(filename):
    lines = csv.reader(open(filename, "rb"))
    dataset = list(lines)
    size = len(dataset)
    for i in range(size):
        for j in dataset[i]:
            dataset[i] = float(j)
    return dataset

def withClass(dataset,size):
    number = []
    for i in range(size):
        vector = dataset[i]
        number.append(vector[-1])
    return list(set(number))

def agroupClass(dataset, size):
    wClass = withClass(dataset,size)
    classData = []
    for i in range(len(wClass)):
        classData.append([wClass[i]])
    for i in range(size):
        vector = dataset[i]
        classData[vector[-1]].append(vector)
    return classData

def summarize(dataset):
    summaries = [(mean(attribute), stdev(attribute)) for attribute in zip(*dataset)]
    del summaries[-1]
    return summaries

def main():
   filename = 'dataset.csv'
   dataset = loadCsv(filename)
   sizeDataset = len(dataset)

    # Holdout
   sizeTrain = 0.67
   train = dataset[:int(sizeTrain*sizeDataset)]
   test = dataset[int(sizeTrain*sizeDataset):sizeDataset]
   train = [[1,20,1], [2,21,0], [3,22,1]]
   classData = agroupClass(train, len(train))
   print(classData)
   print('Separated instances: {0}').format(classData)
if __name__ == '__main__':
   main()


